from django.apps import AppConfig


class EsaiConfig(AppConfig):
    name = 'esai'
