from django.test import TestCase, Client
from .views import *
from .urls import *

class EsaiTest(TestCase):
    
    def test_url_doesnt_exists(self):
        response = Client().get('/this_should_not_exist')
        self.assertTrue(response.status_code, 404)
